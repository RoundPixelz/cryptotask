package com.example.crypto

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.crypto.database.CurrencyDAO
import androidx.lifecycle.Observer
import com.example.crypto.database.CryptoCurrency
import com.example.crypto.database.OrderByHelper
import com.example.crypto.model.CryptoCurrencyToPopulateWith
import com.example.crypto.util.Resource
import com.example.crypto.viewmodel.MainViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.`when` as whenever


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()
    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: MainViewModel
    @Mock
    private lateinit var currencyRepository: CurrencyDAO
    @Mock
    private lateinit var photosResponseObserver: Observer<Resource<List<CryptoCurrencyToPopulateWith>>>

    @Before
    fun setUp() {
        viewModel = MainViewModel(currencyRepository)
    }

    @Test
    fun `when calling for results then return loading`() {
        testCoroutineRule.runBlockingTest {
            viewModel.data.observeForever(photosResponseObserver)
            viewModel.load()
            verify(photosResponseObserver).onChanged(Resource.loading())
            verify(photosResponseObserver).onChanged(Resource.success(listOf()))
        }
    }


    @Test
    fun `when calling for results then return loading and data`() {
        testCoroutineRule.runBlockingTest {

            whenever(currencyRepository.getAll()).thenAnswer {
                listWithCurrency
            }

            viewModel.data.observeForever(photosResponseObserver)
            viewModel.load()
            verify(photosResponseObserver).onChanged(Resource.loading())
            verify(photosResponseObserver).onChanged(Resource.success(modifiedCurrencyList))
        }
    }

    @Test
    fun `when database is empty on load prefill with data`(){
        testCoroutineRule.runBlockingTest {
            whenever(currencyRepository.getAll()).thenReturn (
                emptyList()
            )

            viewModel.load()

            verify(currencyRepository, times(1)).insertAll(
                CryptoCurrency("BTC", "Bitcoin", "BTC"),
                CryptoCurrency("ETH", "Ethereum", "ETH"),
                CryptoCurrency("XRP", "XRP", "XRP"),
                CryptoCurrency("BCH", "Bitcoin Cash", "BCH"),
                CryptoCurrency("LTC", "Litecoin", "LTC"),
                CryptoCurrency("EOS", "EOS", "EOS"),
                CryptoCurrency("BNB", "Binance Coin", "BNB"),
                CryptoCurrency("LINK", "Chainlink", "LINK"),
                CryptoCurrency("NEO", "NEO", "NEO"),
                CryptoCurrency("ETC", "Ethereum Classic", "ETC"),
                CryptoCurrency("ONT", "Ontology", "ONT"),
                CryptoCurrency("CRO", "Crypto.com Chain", "CRO"),
                CryptoCurrency("CUC", "Cucumber", "CUC"),
                CryptoCurrency("USDC", "USD Coin", "USDC"))

        }


        @After
        fun tearDown() {
            viewModel.data.removeObserver(photosResponseObserver)
        }
    }


    companion object{
        val listWithCurrency = listOf(
            CryptoCurrency("BTC", "Bitcoin", "BTC"),
            CryptoCurrency("ETH", "Ethereum", "ETH"),
            CryptoCurrency("XRP", "XRP", "XRP")
        )

        val modifiedCurrencyList = listOf(
            CryptoCurrencyToPopulateWith("B","Bitcoin","BTC"),
            CryptoCurrencyToPopulateWith("E","Ethereum","ETH"),
            CryptoCurrencyToPopulateWith("X","XRP","XRP")
        )

    }
}

