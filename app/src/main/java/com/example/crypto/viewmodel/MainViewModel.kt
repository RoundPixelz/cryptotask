package com.example.crypto.viewmodel

import android.util.Base64
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.crypto.R
import com.example.crypto.database.CryptoCurrency
import com.example.crypto.model.CryptoCurrencyToPopulateWith
import com.example.crypto.database.CurrencyDAO
import com.example.crypto.database.OrderByHelper
import com.example.crypto.util.Resource
import com.example.crypto.view.BaseResourceViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.coroutines.CoroutineContext

class MainViewModel(
    private val currencyDAO: CurrencyDAO
) : BaseResourceViewModel<CryptoCurrencyToPopulateWith>() {

    private var sortLocker: Boolean = false
    var lastSortedOption: OrderByHelper? = null

    fun load() {
        viewModelScope.launch(Dispatchers.IO) {
            sendLoadingState(this)
            if (checkIfDatabaseIsEmpty()) {
                fillData()
            }

            val rawData = currencyDAO.getAll()
            val dataToSend = prepareDataForPresent(rawData)
            sendSuccessState(this, dataToSend)
        }

    }

    fun sort() {
        viewModelScope.launch(Dispatchers.IO) {
            if (sortLocker) sendErrorState(this, R.string.sorting_ongoing, true)
            else {
                sortLocker = true
                sendLoadingState(this)
                delay(5000)
                val rawData = currencyDAO.getAllSortedBySymbol(getSortCriteria())
                if (rawData.isEmpty()) sendErrorState(this, R.string.no_data_to_be_sorted)
                else {
                    val dataToSend = prepareDataForPresent(rawData)
                    sendSuccessState(this, dataToSend)
                }
                sortLocker = false
            }
        }
    }

    private fun checkIfDatabaseIsEmpty(): Boolean = currencyDAO.getAll().isEmpty()


    private fun fillData() {
        currencyDAO.insertAll(
            CryptoCurrency("BTC", "Bitcoin", "BTC"),
            CryptoCurrency("ETH", "Ethereum", "ETH"),
            CryptoCurrency("XRP", "XRP", "XRP"),
            CryptoCurrency("BCH", "Bitcoin Cash", "BCH"),
            CryptoCurrency("LTC", "Litecoin", "LTC"),
            CryptoCurrency("EOS", "EOS", "EOS"),
            CryptoCurrency("BNB", "Binance Coin", "BNB"),
            CryptoCurrency("LINK", "Chainlink", "LINK"),
            CryptoCurrency("NEO", "NEO", "NEO"),
            CryptoCurrency("ETC", "Ethereum Classic", "ETC"),
            CryptoCurrency("ONT", "Ontology", "ONT"),
            CryptoCurrency("CRO", "Crypto.com Chain", "CRO"),
            CryptoCurrency("CUC", "Cucumber", "CUC"),
            CryptoCurrency("USDC", "USD Coin", "USDC")
        )
    }

    private fun prepareDataForPresent(dataToPrepare: List<CryptoCurrency>):
            List<CryptoCurrencyToPopulateWith> {
        return dataToPrepare.map {
            CryptoCurrencyToPopulateWith(
                it.symbol?.get(0).toString(),
                it.name,
                it.symbol
            )
        }
    }

    private fun getSortCriteria(): Int {
        when (lastSortedOption) {
            null -> {
                lastSortedOption = OrderByHelper.ASCENDING
                return OrderByHelper.ASCENDING.ordinal
            }
            OrderByHelper.ASCENDING -> {
                lastSortedOption = OrderByHelper.DESCENDING
                return OrderByHelper.DESCENDING.ordinal
            }
            OrderByHelper.DESCENDING -> {
                lastSortedOption = OrderByHelper.ASCENDING
                return OrderByHelper.ASCENDING.ordinal
            }

        }
    }
}
