package com.example.crypto.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.crypto.database.CryptoCurrency

@Dao
interface CurrencyDAO {
    @Query("SELECT * FROM cryptocurrency")
    fun getAll(): List<CryptoCurrency>

    @Query("SELECT * FROM cryptocurrency WHERE id IN (:currIds)")
    fun loadAllByIds(currIds: IntArray): List<CryptoCurrency>

    @Insert
    fun insertAll(vararg users: CryptoCurrency)

    @Delete
    fun delete(user: CryptoCurrency)

    @Query("DELETE FROM cryptocurrency")
    fun deleteAll()

    @Query("SELECT * FROM cryptocurrency ORDER BY CASE WHEN :orderByHelper = 0 THEN name END ASC, CASE WHEN :orderByHelper = 1 THEN name END DESC")
    fun getAllSortedBySymbol(orderByHelper: Int): List<CryptoCurrency>

}