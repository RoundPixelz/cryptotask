package com.example.crypto.database

enum class OrderByHelper(sqlValue:Int) {
    ASCENDING(0),
    DESCENDING(1)
}
