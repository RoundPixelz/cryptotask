package com.example.crypto.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.crypto.database.CryptoCurrency
import com.example.crypto.database.CurrencyDAO


@Database(entities = [CryptoCurrency::class], version = 1, exportSchema = false)
abstract class CurrencyDatabase : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDAO

}