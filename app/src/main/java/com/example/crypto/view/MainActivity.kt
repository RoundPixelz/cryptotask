package com.example.crypto.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.crypto.viewmodel.MainViewModel
import com.example.crypto.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =  ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            replace(binding.fragmentHolder.id,  CurrencyListFragment())
            addToBackStack(null)
            commit()
        }

        binding.loadButton.setOnClickListener {
            viewModel.load()
        }

        binding.sortButton.setOnClickListener {
            viewModel.sort()
        }
    }

}