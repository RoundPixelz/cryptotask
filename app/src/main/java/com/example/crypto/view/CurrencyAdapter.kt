package com.example.crypto.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.crypto.databinding.CurrencyRowBinding
import com.example.crypto.model.CryptoCurrencyToPopulateWith

class CurrencyAdapter (var list: List<CryptoCurrencyToPopulateWith>) :
    RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val binding = CurrencyRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CurrencyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        with(holder){
            with(list[position]){
                binding.currencySymbol.text = this.currencySymbol
                binding.currencyName.text = this.currencyName
                binding.currencyCode.text = this.currencyCode
            }
        }
    }

    inner class CurrencyViewHolder(val binding: CurrencyRowBinding) :RecyclerView.ViewHolder(binding.root)

    override fun getItemCount() = list.count()
}