package com.example.crypto.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.crypto.util.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

open class BaseResourceViewModel<T> : ViewModel(){

    private val _data = MutableLiveData<Resource<List<T>>>()
    val data: LiveData<Resource<List<T>>>
        get() = _data


    protected fun sendLoadingState(coroutineContext: CoroutineScope) {
        coroutineContext.launch(Dispatchers.Main) {
            _data.value = Resource.loading()
        }
    }

    protected fun sendErrorState(
        coroutineContext: CoroutineScope,
        errorMessageId: Int,
        toastError: Boolean = false
    ) {
        coroutineContext.launch(Dispatchers.Main) {
            _data.value = Resource.error(null, errorMessageId, toastError)

        }
    }

    protected fun sendSuccessState(coroutineContext: CoroutineScope,data: List<T> ){
        coroutineContext.launch(Dispatchers.Main) {
            _data.value = Resource.success(data)
        }
    }
}