package com.example.crypto.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.crypto.databinding.FragmentCurrencyListBinding
import com.example.crypto.util.Status
import com.example.crypto.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class CurrencyListFragment : Fragment() {
    private val currencyViewModel by sharedViewModel<MainViewModel>()
    private val currencyFragmentBinding get() = _currencyFragmentBinding
    private var _currencyFragmentBinding: FragmentCurrencyListBinding? = null
    private val currencyAdapter = CurrencyAdapter(emptyList())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _currencyFragmentBinding = FragmentCurrencyListBinding.inflate(inflater, container, false)
        return currencyFragmentBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currencyFragmentBinding?.currencyList?.adapter = currencyAdapter
        currencyFragmentBinding?.currencyList?.layoutManager = LinearLayoutManager(context)

        currencyViewModel.data.observe(viewLifecycleOwner, { resourceListOfCurrencies ->

            when (resourceListOfCurrencies.status) {
                Status.SUCCESS -> {
                    resourceListOfCurrencies.data?.let {
                        currencyAdapter.list = it
                        currencyAdapter.notifyDataSetChanged()
                        currencyFragmentBinding?.informationText?.visibility = View.GONE
                        currencyFragmentBinding?.loadingBar?.visibility = View.GONE
                        currencyFragmentBinding?.currencyList?.visibility = View.VISIBLE
                    }
                }
                Status.ERROR -> {
                    if (resourceListOfCurrencies.toastMessage) {
                        resourceListOfCurrencies.message?.let {
                            Toast.makeText(activity, this.getString(it), Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        resourceListOfCurrencies.message?.let {
                            currencyFragmentBinding?.informationText?.text =
                                this.getString(it)
                        }
                        currencyFragmentBinding?.informationText?.visibility = View.VISIBLE
                        currencyFragmentBinding?.currencyList?.visibility = View.GONE
                        currencyFragmentBinding?.loadingBar?.visibility = View.GONE
                    }
                }

                Status.LOADING -> {
                    currencyFragmentBinding?.informationText?.visibility = View.GONE
                    currencyFragmentBinding?.currencyList?.visibility = View.GONE
                    currencyFragmentBinding?.loadingBar?.visibility = View.VISIBLE
                }
            }
        })


    }

}