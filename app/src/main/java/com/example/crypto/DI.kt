package com.example.crypto

import android.app.Application
import androidx.room.Room
import com.example.crypto.database.CurrencyDAO
import com.example.crypto.database.CurrencyDatabase
import com.example.crypto.viewmodel.MainViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val databaseModule = module {
    fun provideDatabase(application: Application): CurrencyDatabase {
        return Room.databaseBuilder(application, CurrencyDatabase::class.java, "currencies")
            .fallbackToDestructiveMigration()
            .build()
    }

    fun provideCurrenciesDao(database: CurrencyDatabase): CurrencyDAO {
        return database.currencyDao()
    }

    single { provideDatabase(androidApplication()) }
    single { provideCurrenciesDao(get()) }

}

val viewModelModule = module {
    viewModel { MainViewModel(get()) }
}