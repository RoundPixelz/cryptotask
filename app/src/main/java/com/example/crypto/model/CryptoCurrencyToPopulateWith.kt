package com.example.crypto.model

data class CryptoCurrencyToPopulateWith(
    val currencySymbol: String?,
    val currencyName: String?,
    val currencyCode: String?
)