package com.example.crypto.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}