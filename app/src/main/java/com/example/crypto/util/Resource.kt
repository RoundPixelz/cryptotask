package com.example.crypto.util

data class Resource<out T>(
    val status: Status,
    val data: T?,
    val message: Int?,
    val toastMessage: Boolean = false
) {
    companion object {
        fun <T> success(data: T): Resource<T> =
            Resource(status = Status.SUCCESS, data = data, message = null, toastMessage = false)

        fun <T> error(data: T?, message: Int, toastMessage: Boolean): Resource<T> =
            Resource(status = Status.ERROR, data = data, message = message, toastMessage = toastMessage)

        fun <T> loading(data: T? = null): Resource<T> =
            Resource(status = Status.LOADING, data = data, message = null, toastMessage = false)
    }
}